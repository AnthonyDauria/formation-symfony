<?php

    namespace App\EventListener;

    use App\Entity\User;
    use Doctrine\ORM\Event\LifecycleEventArgs;

    class UserListener 
    {
        /**
        * @param User $user
        * @param LifecycleEventArgs $event
        */
        
        public function postPersist(User $user, LifecycleEventArgs $event): void
        {
            if($user->validated == false){
                $this->mailer->sendConfirmationReservation($user);
            }
        }
    }

?>