<?php
BLINDZ UP
namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    private $hello;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function gethello(): ?int
    {
        return $this->hello;
    }
}
