<?php

namespace ContainerGmWBl2s;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderc80a6 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer829cb = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties2d2f4 = [
        
    ];

    public function getConnection()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getConnection', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getMetadataFactory', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getExpressionBuilder', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'beginTransaction', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getCache', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getCache();
    }

    public function transactional($func)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'transactional', array('func' => $func), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'wrapInTransaction', array('func' => $func), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'commit', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->commit();
    }

    public function rollback()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'rollback', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getClassMetadata', array('className' => $className), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'createQuery', array('dql' => $dql), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'createNamedQuery', array('name' => $name), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'createQueryBuilder', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'flush', array('entity' => $entity), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'clear', array('entityName' => $entityName), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->clear($entityName);
    }

    public function close()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'close', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->close();
    }

    public function persist($entity)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'persist', array('entity' => $entity), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'remove', array('entity' => $entity), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'refresh', array('entity' => $entity), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'detach', array('entity' => $entity), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'merge', array('entity' => $entity), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getRepository', array('entityName' => $entityName), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'contains', array('entity' => $entity), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getEventManager', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getConfiguration', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'isOpen', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getUnitOfWork', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getProxyFactory', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'initializeObject', array('obj' => $obj), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'getFilters', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'isFiltersStateClean', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'hasFilters', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return $this->valueHolderc80a6->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer829cb = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderc80a6) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderc80a6 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderc80a6->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, '__get', ['name' => $name], $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        if (isset(self::$publicProperties2d2f4[$name])) {
            return $this->valueHolderc80a6->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc80a6;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderc80a6;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc80a6;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderc80a6;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, '__isset', array('name' => $name), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc80a6;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderc80a6;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, '__unset', array('name' => $name), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc80a6;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderc80a6;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, '__clone', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        $this->valueHolderc80a6 = clone $this->valueHolderc80a6;
    }

    public function __sleep()
    {
        $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, '__sleep', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;

        return array('valueHolderc80a6');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer829cb = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer829cb;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer829cb && ($this->initializer829cb->__invoke($valueHolderc80a6, $this, 'initializeProxy', array(), $this->initializer829cb) || 1) && $this->valueHolderc80a6 = $valueHolderc80a6;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderc80a6;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderc80a6;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
