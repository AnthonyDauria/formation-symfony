<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerIN04nKi\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerIN04nKi/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerIN04nKi.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerIN04nKi\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerIN04nKi\App_KernelDevDebugContainer([
    'container.build_hash' => 'IN04nKi',
    'container.build_id' => 'c4bd3bbe',
    'container.build_time' => 1635179910,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerIN04nKi');
