<?php

namespace ContainerSWX613D;

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder4e837 = null;
    private $initializer1b7df = null;
    private static $publicProperties33ed1 = [
        
    ];
    public function getConnection()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getConnection', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getConnection();
    }
    public function getMetadataFactory()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getMetadataFactory', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getMetadataFactory();
    }
    public function getExpressionBuilder()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getExpressionBuilder', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getExpressionBuilder();
    }
    public function beginTransaction()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'beginTransaction', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->beginTransaction();
    }
    public function getCache()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getCache', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getCache();
    }
    public function transactional($func)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'transactional', array('func' => $func), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->transactional($func);
    }
    public function wrapInTransaction(callable $func)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'wrapInTransaction', array('func' => $func), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->wrapInTransaction($func);
    }
    public function commit()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'commit', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->commit();
    }
    public function rollback()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'rollback', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->rollback();
    }
    public function getClassMetadata($className)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getClassMetadata', array('className' => $className), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getClassMetadata($className);
    }
    public function createQuery($dql = '')
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'createQuery', array('dql' => $dql), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->createQuery($dql);
    }
    public function createNamedQuery($name)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'createNamedQuery', array('name' => $name), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->createNamedQuery($name);
    }
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->createNativeQuery($sql, $rsm);
    }
    public function createNamedNativeQuery($name)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->createNamedNativeQuery($name);
    }
    public function createQueryBuilder()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'createQueryBuilder', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->createQueryBuilder();
    }
    public function flush($entity = null)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'flush', array('entity' => $entity), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->flush($entity);
    }
    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->find($className, $id, $lockMode, $lockVersion);
    }
    public function getReference($entityName, $id)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getReference($entityName, $id);
    }
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getPartialReference($entityName, $identifier);
    }
    public function clear($entityName = null)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'clear', array('entityName' => $entityName), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->clear($entityName);
    }
    public function close()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'close', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->close();
    }
    public function persist($entity)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'persist', array('entity' => $entity), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->persist($entity);
    }
    public function remove($entity)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'remove', array('entity' => $entity), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->remove($entity);
    }
    public function refresh($entity)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'refresh', array('entity' => $entity), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->refresh($entity);
    }
    public function detach($entity)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'detach', array('entity' => $entity), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->detach($entity);
    }
    public function merge($entity)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'merge', array('entity' => $entity), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->merge($entity);
    }
    public function copy($entity, $deep = false)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->copy($entity, $deep);
    }
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->lock($entity, $lockMode, $lockVersion);
    }
    public function getRepository($entityName)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getRepository', array('entityName' => $entityName), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getRepository($entityName);
    }
    public function contains($entity)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'contains', array('entity' => $entity), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->contains($entity);
    }
    public function getEventManager()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getEventManager', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getEventManager();
    }
    public function getConfiguration()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getConfiguration', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getConfiguration();
    }
    public function isOpen()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'isOpen', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->isOpen();
    }
    public function getUnitOfWork()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getUnitOfWork', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getUnitOfWork();
    }
    public function getHydrator($hydrationMode)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getHydrator($hydrationMode);
    }
    public function newHydrator($hydrationMode)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->newHydrator($hydrationMode);
    }
    public function getProxyFactory()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getProxyFactory', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getProxyFactory();
    }
    public function initializeObject($obj)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'initializeObject', array('obj' => $obj), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->initializeObject($obj);
    }
    public function getFilters()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'getFilters', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->getFilters();
    }
    public function isFiltersStateClean()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'isFiltersStateClean', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->isFiltersStateClean();
    }
    public function hasFilters()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'hasFilters', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return $this->valueHolder4e837->hasFilters();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);
        $instance->initializer1b7df = $initializer;
        return $instance;
    }
    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;
        if (! $this->valueHolder4e837) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder4e837 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
        }
        $this->valueHolder4e837->__construct($conn, $config, $eventManager);
    }
    public function & __get($name)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, '__get', ['name' => $name], $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        if (isset(self::$publicProperties33ed1[$name])) {
            return $this->valueHolder4e837->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4e837;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder4e837;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4e837;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder4e837;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, '__isset', array('name' => $name), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4e837;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder4e837;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, '__unset', array('name' => $name), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4e837;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder4e837;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, '__clone', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        $this->valueHolder4e837 = clone $this->valueHolder4e837;
    }
    public function __sleep()
    {
        $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, '__sleep', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
        return array('valueHolder4e837');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1b7df = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1b7df;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1b7df && ($this->initializer1b7df->__invoke($valueHolder4e837, $this, 'initializeProxy', array(), $this->initializer1b7df) || 1) && $this->valueHolder4e837 = $valueHolder4e837;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder4e837;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder4e837;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
